***(-----------------------------------------------------------------

Module      :  term-indexing
Stability   :  unstable
Portability :  portable

Term Indexing

---------------------------------------------------------------------)

--- Tuple: (Clause Id, Annotated Clause Id, Matching Subterm)
fmod LEAF is
  pr META-TERM .
  pr 4TUPLE{Nat,Nat,Term,Position} * (sort Tuple{Nat,Nat,Term,Position} to Leaf) .
endfm

view Leaf from TRIV to LEAF is sort Elt to Leaf .  endv

fmod S-NODE is
  inc LEAF .
  pr SET{Leaf} * ( sort Set{Leaf} to LeafSet
                 , sort NeSet{Leaf} to NeLeafSet) .

  sorts SNode . --- symbol node

  op *-L->_ : NeLeafSet -> SNode [ctor] . --- variable
  op _-L->_ : Constant NeLeafSet -> SNode [ctor] . --- constant
  op _-L->_ : Qid NeLeafSet -> SNode [ctor] .  --- AC symbol
endfm

view LeafSet from TRIV to S-NODE is sort Elt to LeafSet . endv
view NeLeafSet from TRIV to S-NODE is sort Elt to NeLeafSet . endv
view SNode from TRIV to S-NODE is sort Elt to SNode .  endv

fmod S-NODE-SET is
  inc S-NODE .
  pr SET{SNode} * ( sort Set{SNode} to SNodeSet
                  , sort NeSet{SNode} to NeSNodeSet) .
endfm

view SNodeSet from TRIV to S-NODE-SET is sort Elt to SNodeSet .  endv
view NeSNodeSet from TRIV to S-NODE-SET is sort Elt to NeSNodeSet .  endv

fmod P-NODE is
  inc S-NODE-SET .
  pr MAP{Nat,NeSNodeSet} * 
     ( sort Map{Nat,NeSNodeSet} to PNode
     , sort Entry{Nat,NeSNodeSet} to PNodeEntry) .

  op _-P->_  : Qid PNode -> SNode [ctor] .
endfm

fmod TERM-INDEXING is
  pr P-NODE .
  pr CONSTANTS .
  pr TERM-POSITION-SET .

  sort TIndex . --- Indexing Tree

  subsorts SNodeSet < TIndex .

  vars T T1 T2 : Term .
  var NeTL : NeTermList .
  var C : Constant .
  var V : Variable .
  var Q : Qid .
  vars I I1 I2 I3 : Nat .
  vars SS SS1 SS2 : SNodeSet .
  var LS : LeafSet .
  var NeLS : NeLeafSet .
  vars PN PN1 PN2 : PNode .
  var P : Position .
  var NeTPS : NeTermPositionSet .
  var TPS : TermPositionSet .
  var TP : TermPosition .
  var QS : QidSet . --- AvC-SYMBOLS

  --- indexTerm - we index a Triple (Nat,Nat,Term) in the TIndex
  op indexTerm : Nat Nat QidSet TermPositionSet TIndex -> TIndex .
  eq indexTerm(I1,I2,QS,(empty).TermPositionSet,SS) = SS .
  eq indexTerm(I1,I2,QS,(T, P),SS) = indexTerm(T,I1,I2,QS,T,P,SS) .
  eq indexTerm(I1,I2,QS,(TP, NeTPS),SS) = indexTerm(I1,I2,QS,NeTPS,indexTerm(p1 TP,I1,I2,QS,p1 TP,p2 TP,SS)) .

  --- indexTerm - we index a Triple (Nat,Nat,Term) following the tree
  --- structure
  op indexTerm : Term Nat Nat QidSet Term Position SNodeSet -> SNodeSet .
  eq indexTerm(C, I1, I2, QS, T, P, ((C -L-> NeLS) , SS)) = (C -L-> indexTerm(T, P, I1, I2, NeLS)) , SS .
  eq indexTerm(C, I1, I2, QS, T, P, SS) = (C -L-> (I1,I2,T,P)) , SS [owise] .
  eq indexTerm(Q[NeTL], I1, I2, QS, T, P, ((Q -L-> NeLS) , SS)) = (Q -L-> indexTerm(T, P, I1, I2, NeLS)) , SS . --- AC Term
  eq indexTerm(Q[NeTL], I1, I2, QS, T, P, ((Q -P-> PN) , SS)) = (Q -P-> indexTermList(NeTL, I1, I2, QS, T, P, PN,1)) , SS . --- Non-AC Term
  eq indexTerm(Q[NeTL], I1, I2, QS, T, P, SS) 
   = if Q in QS then
       (Q -L-> (I1,I2,T,P)) , SS
     else 
       (Q -P-> indexTermList(NeTL,I1,I2,QS,T,P,(empty).PNode,1)) , SS
     fi [owise] .
  eq indexTerm(V, I1, I2, QS, T, P, ((*-L-> NeLS) , SS)) = (*-L-> indexTerm(T, P, I1, I2, NeLS)) , SS .
  eq indexTerm(V, I1, I2, QS, T, P, SS) = (*-L-> (I1,I2,T,P)) , SS [owise] .

  --- indexTerm - index term in a leaf set
  op indexTerm : Term Position Nat Nat LeafSet -> LeafSet .
  eq indexTerm(T, P, I1, I2, ((I1, I2, T, P) , LS)) = (I1, I2, T, P) , LS .
  eq indexTerm(T, P, I1, I2, LS) = (I1, I2, T, P) , LS [owise] .

  --- indexTermList - index in PNode
  op indexTermList : TermList Nat Nat QidSet Term Position PNode Nat -> PNode .
  eq indexTermList(T1, I1, I2, QS, T2, P, ((I3 |-> SS) , PN), I3) = ((I3 |-> indexTerm(T1, I1, I2, QS, T2, P, SS)) , PN) .
  eq indexTermList(T1, I1, I2, QS, T2, P, PN, I3) = ((I3 |-> indexTerm(T1, I1, I2, QS, T2, P, (empty).SNodeSet)) , PN) [owise] .
  eq indexTermList((T1, NeTL), I1, I2, QS, T2, P, ((I3 |-> SS) , PN), I3) = indexTermList(NeTL, I1, I2, QS, T2, P, ((I3 |-> indexTerm(T1, I1, I2, QS, T2, P, SS)) , PN), s I3) .
  eq indexTermList((T1, NeTL), I1, I2, QS, T2, P, PN, I3) = indexTermList(NeTL, I1, I2, QS, T2, P, ((I3 |-> indexTerm(T1, I1, I2, QS, T2, P, (empty).SNodeSet)) , PN), s I3) [owise] .

  --- removeIndex - given an index I, we remove any triple (I,Nat,Term)
  op removeIndex : Nat TIndex -> TIndex .
  eq removeIndex(I,((C -L-> NeLS) , SS)) = if removeIndex(I, NeLS) == (empty).LeafSet then removeIndex(I, SS) else ((C -L-> removeIndex(I, NeLS)) , removeIndex(I, SS)) fi .
  eq removeIndex(I, ((Q -L-> NeLS) , SS)) = if removeIndex(I, NeLS) == (empty).LeafSet then removeIndex(I, SS) else ((Q -L-> removeIndex(I, NeLS)) , removeIndex(I,SS)) fi . --- AC Term
  eq removeIndex(I, ((Q -P-> PN1) , SS)) = if removeIndex(I, PN1) == (empty).PNode then removeIndex(I, SS) else ((Q -P-> removeIndex(I, PN1)) , removeIndex(I,SS)) fi . --- Non-AC Term
  eq removeIndex(I, ((*-L-> NeLS) , SS)) = if removeIndex(I, NeLS) == (empty).LeafSet then removeIndex(I, SS) else ((*-L-> removeIndex(I, NeLS)) , removeIndex(I, SS)) fi .
  eq removeIndex(I, SS) = SS [owise] .

  --- removeIndex - remove index in a leaf set
  op removeIndex : Nat LeafSet -> LeafSet .
  eq removeIndex(I1, ((I1, I2, T, P) , LS)) = removeIndex(I1, LS) .
  eq removeIndex(I, LS) = LS [owise] .

  --- removeIndex - remove index in a PNode
  op removeIndex : Nat PNode -> PNode .
  eq removeIndex(I1, ((I2 |-> SS1) , PN)) = if removeIndex(I1,SS1) == (empty).SNodeSet then (empty).PNode else (if SS1 == removeIndex(I1,SS1) then ((I2 |-> SS1) , PN) else ((I2 |-> removeIndex(I1,SS1)) , removeIndex(I1,PN)) fi) fi .
  eq removeIndex(I,PN) = PN [owise] .

  --- unifiable - we return the set of possible unifiable triples
  op unifiable : Term TIndex -> LeafSet .
  eq unifiable(C, ((C -L-> NeLS) , SS)) = NeLS , unifiable(C,SS) .
  eq unifiable(C, ((*-L-> NeLS) , SS)) = NeLS , unifiable(C,SS) .
  eq unifiable(Q[NeTL], ((Q -L-> NeLS) , SS)) = NeLS , unifiable(Q[NeTL],SS) . --- AC Term
  eq unifiable(Q[NeTL], ((Q -P-> PN) , SS)) = unifiable(NeTL,PN,1) , unifiable(Q[NeTL],SS) . --- Non-AC Term
  eq unifiable(Q[NeTL], ((*-L-> NeLS) , SS)) = NeLS , unifiable(Q[NeTL],SS) .
  eq unifiable(V, ((C -L-> NeLS) , SS)) = NeLS , unifiable(V,SS) .
  eq unifiable(V, ((Q -L-> NeLS) , SS)) = NeLS , unifiable(V,SS) .
  eq unifiable(V, ((Q -P-> PN) , SS)) = unifiables(V,PN) , unifiable(V,SS) .
  eq unifiable(V, ((*-L-> NeLS) , SS)) = NeLS , unifiable(V,SS) .
  eq unifiable(T, SS) = (empty).LeafSet [owise] .

  --- unifiable - intersection of all unifiable arguments
  op unifiable : TermList PNode Nat -> LeafSet .
  eq unifiable(T, ((I |-> SS) , PN), I) = unifiable(T,SS) .
  eq unifiable((T , NeTL), ((I |-> SS) , PN), I) = intersection(unifiable(T,SS), unifiable(NeTL,PN, s I)) .
  eq unifiable(NeTL, PN, I) = (empty).LeafSet [owise] .

  --- unifiable - get all leafs
  op unifiables : Variable PNode -> LeafSet .
  eq unifiables(V, (empty).PNode) = (empty).LeafSet .
  eq unifiables(V, ((I |-> SS) , PN)) = unifiable(V,SS) .

***(
  op myTtree : -> TIndex .
  eq myTtree = indexTerm(4, 2, 'f['X:Nat,'Y:Nat], indexTerm(4, 1, 'f['g['X:Nat,'c.Nat],'b.Nat], indexTerm(3, 1, 'f['g['a.Nat,'b.Nat],'c.Nat], indexTerm(2, 1, 'f['g['X:Nat,'b.Nat],'Y:Nat], indexTerm(1, 1, 'f['g['a.Nat,'X:Nat],'c.Nat], (empty).SNodeSet))))) .
)
endfm

view TIndex from TRIV to TERM-INDEXING is sort Elt to TIndex .  endv
