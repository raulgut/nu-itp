***(-----------------------------------------------------------------

Module      :  configuration
Stability   :  unstable
Portability :  portable 

ERTP configuration elements

---------------------------------------------------------------------)

fmod RTP-CONFIGURATION is
  pr CURRENT-CLAUSES .
  pr NAT-SET .
  pr SIDE-TABLE .
  pr ORDERED-EXTENDED-WEIGHTED-CLAUSE-LIST .
  pr NAT+ .

  sorts Configuration
	InputModule
	Ordering
        CurrentClauses
        DeprecatedClauses 
        SimplificationClauses
        SideTable
        MainTable 
        PreprocessedQueue
        UnprocessedClauses
        Conjecture
        Lemma
        Step 
        MaxStep 
        Verbose .

  subsorts InputModule CurrentClauses DeprecatedClauses SimplificationClauses 
           SideTable MainTable PreprocessedQueue UnprocessedClauses 
           Conjecture Lemma Step MaxStep Ordering Verbose < Configuration .

  op empty : -> Configuration .
--- _,_ - a configuration is a set of configurations
  op _,_ : Configuration Configuration -> Configuration [ctor comm assoc id: empty format (nss d nss d)] .

  --- Input Module
  op M`: _ : Module -> InputModule [ctor] .
  --- Ordering
  op O`: _ : Module -> Ordering [ctor] .
  --- Current Clauses
  op CC`: _ : IEWClauseTable -> CurrentClauses [ctor] .
  op CC`: _ : EWClauseTable -> CurrentClauses [ctor] .
  --- Conjectures - Current Clauses
  op CjCC`: _ : IEWClauseTable -> CurrentClauses [ctor] .
  op CjCC`: _ : EWClauseTable -> CurrentClauses [ctor] .
  --- Lemmas - Current Clauses
  op LemCC`: _ : IEWClauseTable -> CurrentClauses [ctor] .
  op LemCC`: _ : EWClauseTable -> CurrentClauses [ctor] .
  --- Deprecated Clauses
  op DC`: _ : EWClauseTable -> DeprecatedClauses [ctor] .
  --- Conjectures - Deprecated Clauses
  op CjDC`: _ : EWClauseTable -> DeprecatedClauses [ctor] .
  --- Simplification Rules
  op SR`: _ : NatSet -> SimplificationClauses [ctor] .
  --- Conjectures - Simplification Rules
  op CjSR`: _ : NatSet -> SimplificationClauses [ctor] .
  --- Lemmas - Simplification Rules
  op LemSR`: _ : NatSet -> SimplificationClauses [ctor] .
  --- Side Table
  op ST`: _ : STable -> SideTable [ctor] .
  op ST`: _ : AWClauseTable -> SideTable [ctor] .
  --- Conjectures - Side Table
  op CjST`: _ : STable -> SideTable [ctor] .
  op CjST`: _ : AWClauseTable -> SideTable [ctor] .
  --- Lemmas - Side Table
  op LemST`: _ : STable -> SideTable [ctor] .
  op LemST`: _ : AWClauseTable -> SideTable [ctor] .
  --- Main Table
  op MT`: _ : MTable -> MainTable [ctor] .
  op MT`: _ : AWClauseTable -> SideTable [ctor] .
  --- Conjectures - Main Table
  op CjMT`: _ : MTable -> MainTable [ctor] .
  op CjMT`: _ : AWClauseTable -> SideTable [ctor] .
  --- Preprocessed Queue
  op PQ`: _ : OEWClauseList -> PreprocessedQueue [ctor] .
  --- Unprocessed Clauses
  op UC`: _ : EWClauseSet -> UnprocessedClauses [ctor] .
  --- Conjecture
  op Cj`: _ : EWClauseSet -> Conjecture [ctor] .
  --- Lemma
  op Lem`: _ : EWClauseSet -> Lemma [ctor] .
  --- Resolution Step
  op Step`: _ : Nat -> Step [ctor] .
  --- Maximum Resolution Step
  op MaxStep`: _ : Nat+ -> MaxStep [ctor] .
  --- Verbosity
  op Verbose`: _ : Bool -> Verbose [ctor] .

  op initInputModule : Module -> InputModule .
  op initOrdering : Module -> Ordering .
  op initCurrentClauses   : Module -> CurrentClauses .
  op initCjCurrentClauses   : Module Nat -> CurrentClauses .
  op initLemCurrentClauses   : Module Nat -> CurrentClauses .
  op initDeprecateClauses : -> DeprecatedClauses .
  op initCjDeprecateClauses : -> DeprecatedClauses .
  op initSimplificationClauses : -> SimplificationClauses .
  op initCjSimplificationClauses : -> SimplificationClauses .
  op initLemSimplificationClauses : -> SimplificationClauses .
  op initSideTable : -> SideTable .
  op initCjSideTable : -> SideTable .
  op initLemSideTable : -> SideTable .
  op initMainTable : -> MainTable .
  op initCjMainTable : -> MainTable .
  op initPreprocessedQueue  : -> PreprocessedQueue .
  op initUnprocessedClauses : EWClauseSet -> UnprocessedClauses .
  op initConjecture : EWClauseSet -> Conjecture .
  op initLemma : EWClauseSet -> Lemma .
  op initStep : Nat -> Step .
  op initMaxStep : Nat+ -> MaxStep .
  op initVerbose : Bool -> Verbose .

  var CSTM : EWClauseSet .
  var N : Nat .
  var N+ : Nat+ .
  var M : Module .
  var B : Bool .
  var C : Configuration .

  eq initInputModule(M) = (M : M) .
  eq initOrdering(M) = (O : aac-compWithOrd(M)) .
  eq initCurrentClauses(M) = (CC : createITable(M)) .
  eq initCjCurrentClauses(M,N) = (CjCC : createITableIndex(M,N)) .
  eq initLemCurrentClauses(M,N) = (LemCC : createITableIndex(M,N)) .
  eq initDeprecateClauses = (DC : createTable) .
  eq initCjDeprecateClauses = (CjDC : createTable) .
  eq initSimplificationClauses = (SR : (empty).NatSet) .
  eq initCjSimplificationClauses = (CjSR : (empty).NatSet) .
  eq initLemSimplificationClauses = (LemSR : (empty).NatSet) .
  eq initSideTable = (ST : (createTable,(empty).TIndex,(empty).TIndex)) .
  eq initCjSideTable = (CjST : (createTable,(empty).TIndex,(empty).TIndex)) .
  eq initLemSideTable = (LemST : (createTable,(empty).TIndex,(empty).TIndex)) .
  eq initMainTable = (MT : (createTable,(empty).TIndex)) .
  eq initCjMainTable = (CjMT : (createTable,(empty).TIndex)) .
  eq initPreprocessedQueue = (PQ : (nil).OEWClauseList) .
  eq initUnprocessedClauses(CSTM) = (UC : CSTM) .
  eq initConjecture(CSTM) = (Cj : CSTM) .
  eq initLemma(CSTM) = (Lem : CSTM) .
  eq initStep(N) = (Step : N) .
  eq initMaxStep(N+) = (MaxStep : N+) .
  eq initVerbose(B) = (Verbose : B) .

  op hasCj : Configuration -> Bool .
  eq hasCj ((Cj : CSTM) , C) = true .
  eq hasCj (C) = false [owise] .

  op hasLem : Configuration -> Bool .
  eq hasLem ((Lem : CSTM) , C) = true .
  eq hasLem (C) = false [owise] .
endfm

fmod CLAUSE-INFO-SIMPLIFICATION is
  pr RTP-CONFIGURATION .
  pr 3TUPLE{EWClauseTable,EWClauseTable,ClauseInfo} .

  var C : WClause .
  var NeCTS : NeEWClauseSet .
  var I : Nat .
  vars CC DC : EWClauseTable .
  var FVI : FVIndex .
  var Tr : ClauseInfo .

  op clauseInfoSimplification : EWClauseTable EWClauseTable EWClause EWClauseSet -> Tuple{EWClauseTable,EWClauseTable,ClauseInfo} . 
  eq clauseInfoSimplification(CC, DC, C from Tr, C from Tr) = (CC, DC, Tr) .
  eq clauseInfoSimplification(CC, DC, C from Tr, (empty).EWClauseSet) = (CC, DC, Tr) .
  eq clauseInfoSimplification(CC, DC, C from Tr, NeCTS) = (incMaxElem(CC), addInPosition((getMaxElem(CC),C from Tr), DC), simplified(getMaxElem(CC))) [owise] .

endfm

fmod CLAUSE-INFO-REWRITING is
  pr RTP-CONFIGURATION .
  pr 3TUPLE{EWClauseTable,EWClauseTable,ClauseInfo} .

  vars C C1 C2 : WClause .
  var NeCTS : NeEWClauseSet .
  var I : Nat .
  vars CC DC : EWClauseTable .
  var FVI : FVIndex .
  var Tr : ClauseInfo .

  op clauseInfoRewriting : EWClauseTable EWClauseTable EWClause EWClauseSet -> Tuple{EWClauseTable,EWClauseTable,ClauseInfo} . 
  eq clauseInfoRewriting(CC, DC, C from Tr,C from Tr) = (CC, DC, Tr) .
  eq clauseInfoRewriting(CC, DC, C from Tr, (empty).EWClauseSet) = (CC, DC, Tr) .
  eq clauseInfoRewriting(CC, DC, C from Tr, NeCTS) = (incMaxElem(CC), addInPosition((getMaxElem(CC),C from Tr), DC), rewritten(getMaxElem(CC))) [owise] .

endfm