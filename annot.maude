***(-----------------------------------------------------------------

Module      :  annot
Stability   :  unstable
Portability :  portable

Marking on clauses

---------------------------------------------------------------------)

--- Annotated sequents 
fmod ANNOT-WEIGHTED-CLAUSE is   
  pr WEIGHT-WEIGHTED-CLAUSE .
    
  sorts AAtom AWClause .

  op [_]=_ : Term Term -> AAtom [ctor] .
  op {_}=_ : Term Term -> AAtom [ctor] .
  op error : -> [AAtom] .

  op __ : AAtom WClause -> AWClause [ctor] .  --- for main table
  op __ : WClause AAtom -> AWClause [ctor] .  --- for side table
  op error : -> [AWClause] .

  vars T T1 T2 : Term .
  vars AA AA1 AA2 : AAtom .
  var GM : WAtomMagma .
  var DT : AtomMagma .
  var N : Nat . 
  vars AC1 AC2 : AWClause .

--- deannot - from AClause to Clause
  op deannot : AWClause -> WClause .
  eq deannot(AA (GM => DT)) = wdeannot(AA) , GM => DT .
  eq deannot((GM => DT) AA) = GM => DT , deannot(AA) .

  op deannot : AAtom -> Atom .
  eq deannot(([T1] = T2)) = T1 = T2 .
  eq deannot(({T1} = T2)) = T1 = T2 .

  op wdeannot : AAtom -> WAtom .
  eq wdeannot(([T1] = T2)) = (weight(T1) + weight(T2)) T1 = T2 .
  eq wdeannot(({T1} = T2)) = (weight(T1) + weight(T2)) T1 = T2 .

--- equality relation for AAtoms
  op _===_ : AAtom AAtom -> Bool [comm] .
  eq AA1  === AA2 = deannot(AA1) === deannot(AA2) .

--- equality relation for AClauses
  op _===_ : AWClause AWClause -> Bool [comm] .
  eq AC1  === AC2 = deannot(AC1) === deannot(AC2) .
endfm

view AAtomEq from EQUALITY-RELATION to ANNOT-WEIGHTED-CLAUSE is 
  sort Elt to AAtom .  
  op _===_ : Elt Elt -> Bool to _===_ .
endv
view AWClauseEq from EQUALITY-RELATION to ANNOT-WEIGHTED-CLAUSE is 
  sort Elt to AWClause .  
  op _===_ : Elt Elt -> Bool to _===_ .
endv
view AWClause from TRIV to ANNOT-WEIGHTED-CLAUSE is sort Elt to AWClause .  endv

fmod ANNOT-WEIGHTED-CLAUSE-UTIL is
  pr ANNOT-WEIGHTED-CLAUSE .
  pr WEIGHTED-CLAUSE-UTIL .
  
  var AC : AWClause .
  vars T1 T2 : Term .
  var M : Module .

  --- used for unification purposses to avoid wrong renamings.
  op getMaxV# : AWClause -> Nat . ---[memo] .
  eq getMaxV#(AC) = getMaxV#(deannot(AC)) .

  --- flat - flat marked term
  op flat : Module AAtom -> AAtom .
  eq flat(M, ([T1] = T2)) = [flat(M, T1)] = T2 .
  eq flat(M, ({T1} = T2)) = {flat(M, T1)} = T2 .

endfm

fmod ANNOT-WEIGHTED-CLAUSE-SET is
  pr ANNOT-WEIGHTED-CLAUSE .
--- annotated sequent magma 
  pr SET{AWClause} * (sort Set{AWClause} to AWClauseSet,
                     sort NeSet{AWClause} to NeAWClauseSet) . 

  vars AM DT : AtomMagma .
  vars GM GM' : WAtomMagma .  
  var AC : AWClause .
  var ACS : AWClauseSet .
  vars AA AA1 AA2 : AAtom .  
  var C : WClause .
  vars AC1 AC2 : AWClause .
  var Tr : Nat .
  var N : Nat .

  ceq AC1 , AC2 , ACS = AC1 , ACS 
  if AC1 === AC2 .

--- Auxiliary set difference defined in pg 149
  op _\_ : AWClauseSet WClause -> AWClauseSet .
  eq (empty).AWClauseSet \ C = (empty).AWClauseSet .  
  eq AC , ACS \ C = if deannot(AC) == C then ACS else AC , (ACS \ C) fi .
endfm

view AWClauseSet from TRIV to ANNOT-WEIGHTED-CLAUSE-SET is sort Elt to AWClauseSet .  endv
view NeAWClauseSet from TRIV to ANNOT-WEIGHTED-CLAUSE-SET is sort Elt to NeAWClauseSet .  endv

fmod ANNOT-WEIGHTED-CLAUSE-SET-UTIL is
  pr ANNOT-WEIGHTED-CLAUSE-UTIL .
  pr ANNOT-WEIGHTED-CLAUSE-SET .
  
  var AA : AAtom .
  var AC : AWClause .
  var NeACS : NeAWClauseSet .
  var GM : WAtomMagma .
  var DT : AtomMagma .
  var M : Module .

  --- flat - flat marked term
  op flat : Module AWClauseSet -> AWClauseSet .
  eq flat(M, (empty).AWClauseSet) = (empty).AWClauseSet .
  eq flat(M, AA (GM => DT)) = flat(M, AA) (GM => DT) .
  eq flat(M, (GM => DT) AA) = (GM => DT) flat(M, AA) .
  eq flat(M, (AC , NeACS)) = flat(M, AC) , flat(M, NeACS) .
endfm
