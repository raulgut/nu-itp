***(-----------------------------------------------------------------

Module      :  rewriting
Stability   :  unstable
Portability :  portable 

Clause rewriting

---------------------------------------------------------------------)

--- Main module
fmod WEIGHTED-CLAUSE-REWRITING is
  pr META-LEVEL .
  pr ANNOT-WEIGHTED-CLAUSE-SET .
  pr THEORY .
  pr VARIABLE-SET .
  pr EXTENDED-WEIGHTED-CLAUSE-SET .
---  pr CONSTANTS .

  vars M M1 M2 : Module .
  var ACS : AWClauseSet .
  var AC : AWClause .
  var NeACS : NeAWClauseSet .
  vars GM WAM : WAtomMagma .
  vars DT AM : AtomMagma .
  var NeAM : NeAtomMagma .
  var NeGM : NeWAtomMagma .
  var NeWAM : NeWAtomMagma .
  var A : Atom .
  var WA : WAtom .
  vars T T1 T2 : Term .
  var N : Nat .
  var EqS   : EquationSet .
  var TType : Type . 
  var CT : EWClause .
  var NeCTS : NeEWClauseSet .
  var Tr : ClauseInfo .

--- rewrite - rewrite each term of a set of clauses
  op rewrite : Module AWClauseSet EWClauseSet -> EWClauseSet . --- [memo] .
  eq rewrite(M,ACS,(empty).EWClauseSet) = (empty).EWClauseSet .
  eq rewrite(M,ACS,(GM => DT) from Tr) = rewrite(M,ACS,(GM => DT)) from Tr .
  eq rewrite(M,ACS,(CT , NeCTS)) = rewrite(M,ACS,CT) , rewrite(M,ACS,NeCTS) .

--- rewrite - rewrite each term of a clause
  op rewrite : Module AWClauseSet WClause -> WClause . --- [memo] .
  eq rewrite(M,ACS,(GM => DT)) = rewrite(M,ACS,GM,GM) => rewrite(M,ACS,GM,DT) .

--- rewrite - rewrite each term of a set of atoms
  op rewrite : Module AWClauseSet WAtomMagma AtomMagma -> AtomMagma . --- [memo] .
  eq rewrite(M,ACS, WAM, (empty).AtomMagma) = (empty).AtomMagma .
  eq rewrite(M,ACS, WAM, (A , NeAM)) = rewrite(M,ACS,WAM,A) , rewrite(M,ACS,WAM,NeAM) .
  eq rewrite(M,ACS, WAM, (T1 = T2)) = rewrite(M,ACS,WAM,T1) = rewrite(M,ACS,WAM,T2) .

--- rewrite - rewrite each term of a set of atoms
  op rewrite : Module AWClauseSet WAtomMagma WAtomMagma -> WAtomMagma . --- [memo] .
  eq rewrite(M,ACS, WAM, (empty).WAtomMagma) = (empty).WAtomMagma .
  eq rewrite(M,ACS, WAM, (WA , NeWAM)) = rewrite(M,ACS,WAM,WA) , rewrite(M,ACS,WAM,NeWAM) .
  eq rewrite(M,ACS, WAM, (N)(T1 = T2)) = (N)(rewrite(M,ACS,WAM,T1) = rewrite(M,ACS,WAM,T2)) .

--- rewrite - rewrite a term
  op rewrite : Module AWClauseSet WAtomMagma Term -> Term . --- [memo] .
  eq rewrite(M,(empty).AWClauseSet,WAM, T)
  = T .
  eq rewrite(M1,NeACS,WAM,T1) 
  = getTerm(metaReduce(addEqs(M1,fromACSToEqs(NeACS, WAM)),T1)) .

--- fromACSToEqs - from clauses (GM => IT1 = IT2) to rules (eq IT1
--- = IT2 if C) .
---
--- *note that if IT1 > IT2 then vars(IT2) subseteq vars(IT1) and if
--- IT1 = IT2 > GM then vars(GM) subseteq vars(IT1 = IT2)
  op fromACSToEqs : AWClauseSet WAtomMagma -> EquationSet . --- [memo] .
  eq fromACSToEqs((empty).AWClauseSet,WAM) = none .
  eq fromACSToEqs((AC , NeACS),WAM) = fromACSToEqs(AC,WAM) fromACSToEqs(NeACS,WAM) .
  eq fromACSToEqs((((empty).WAtomMagma => (empty).AtomMagma) ({T1} = T2)),WAM)
   = (eq T1 = T2 [none] .) .
  eq fromACSToEqs((((empty).WAtomMagma => (empty).AtomMagma) ([T1] = T2)),WAM)
   = if (((getVars(T2) \ getVars(T1)) == none) and (isVar(T1) =/= true))
     then
       (ceq T1 = T2 if ('_>v_['wrapToTerm['`[`[_`]`][T1]],'wrapToTerm['`[`[_`]`][T2]]] = 'true.OBool) [none] .)
     else
       none
     fi .
  eq fromACSToEqs(((NeGM => (empty).AtomMagma) ({T1} = T2)),WAM)
   = (ceq T1 = T2 if ('_subseteq_[constrAM(NeGM),constrAM(WAM)] = 'true.OBool) [none] .) .
  eq fromACSToEqs(((NeGM => (empty).AtomMagma) ([T1] = T2)),WAM)
   = if (((getVars(T2) \ getVars(T1)) == none) and (isVar(T1) =/= true))
     then
       (ceq T1 = T2 if ('_>v_['wrapToTerm['`[`[_`]`][T1]],'wrapToTerm['`[`[_`]`][T2]]] = 'true.OBool) /\ ('_subseteq_[constrAM(NeGM),constrAM(WAM)] = 'true.OBool) [none] .)
     else
       none
     fi .
  eq fromACSToEqs(AC,WAM) = none [owise] .

  --- constrAM - from WAtomMagma into theory AtomMagma
  op constrAM : WAtomMagma -> Term .
  eq constrAM((empty).WAtomMagma) = 'empty.AtomMagma .
  eq constrAM((WA , NeWAM)) = '_`,_[constrAM(WA),constrAM(NeWAM)] .
  eq constrAM((N)(T1 = T2)) = '_=_[T1,T2] .

--- cheap-rewrite - cheap version of rewriting
  op cheap-rewrite : Module AWClauseSet WClause -> WClause . --- [memo] .
  eq cheap-rewrite(M,ACS,(GM => DT)) = cheap-rewrite(M,ACS,GM,GM) => cheap-rewrite(M,ACS,GM,DT) .

--- cheap-rewrite - rewrite each term of a set of atoms
  op cheap-rewrite : Module AWClauseSet WAtomMagma AtomMagma -> AtomMagma . --- [memo] .
  eq cheap-rewrite(M,ACS, WAM, (empty).AtomMagma) = (empty).AtomMagma .
  eq cheap-rewrite(M,ACS, WAM, (A , NeAM)) = cheap-rewrite(M,ACS,WAM,A) , cheap-rewrite(M,ACS,WAM,NeAM) .
  eq cheap-rewrite(M,ACS, WAM, (T1 = T2)) = cheap-rewrite(M,ACS,WAM,T1) = cheap-rewrite(M,ACS,WAM,T2) .

--- cheap-rewrite - rewrite each term of a set of atoms
  op cheap-rewrite : Module AWClauseSet WAtomMagma WAtomMagma -> WAtomMagma . --- [memo] .
  eq cheap-rewrite(M,ACS, WAM, (empty).WAtomMagma) = (empty).WAtomMagma .
  eq cheap-rewrite(M,ACS, WAM, (WA , NeWAM)) = cheap-rewrite(M,ACS,WAM,WA) , cheap-rewrite(M,ACS,WAM,NeWAM) .
  eq cheap-rewrite(M,ACS, WAM, (N)(T1 = T2)) = (N)(cheap-rewrite(M,ACS,WAM,T1) = cheap-rewrite(M,ACS,WAM,T2)) .

--- cheap-rewrite - rewrite a term
  op cheap-rewrite : Module AWClauseSet WAtomMagma Term -> Term . --- [memo] .
  eq cheap-rewrite(M,(empty).AWClauseSet,WAM, T)
  = T .
  eq cheap-rewrite(M1,NeACS,WAM,T1) 
  = getTerm(metaReduce(addEqs(M1,fromCheapACSToEqs(NeACS, WAM)),T1)) .

--- fromCheapACSToEqs - from clauses (GM => IT1 = IT2) to rules (eq IT1
--- = IT2 if C) .
--- we avoid ordering constraints
  op fromCheapACSToEqs : AWClauseSet WAtomMagma -> EquationSet . --- [memo] .
  eq fromCheapACSToEqs((empty).AWClauseSet,WAM) = none .
  eq fromCheapACSToEqs((AC , NeACS),WAM) = fromCheapACSToEqs(AC,WAM) fromCheapACSToEqs(NeACS,WAM) .
  eq fromCheapACSToEqs((((empty).WAtomMagma => (empty).AtomMagma) ({T1} = T2)),WAM)
   = (eq T1 = T2 [none] .) .
  eq fromCheapACSToEqs((((empty).WAtomMagma => (empty).AtomMagma) ([T1] = T2)),WAM)
   = none .
  eq fromCheapACSToEqs(((NeGM => (empty).AtomMagma) ({T1} = T2)),WAM)
   = (ceq T1 = T2 if ('_subseteq_[constrAM(NeGM),constrAM(WAM)] = 'true.OBool) [none] .) .
  eq fromCheapACSToEqs(((NeGM => (empty).AtomMagma) ([T1] = T2)),WAM)
   = none .
  eq fromCheapACSToEqs(AC,WAM) = none [owise] .

endfm

fmod REWRITABLES is
  pr 2TUPLE{NatSet,EWClauseSet} 
    * (op ((_,_)) : NatSet EWClauseSet -> Tuple{NatSet, EWClauseSet} to ((_;_))) .

  pr SELECTION .
  pr WEIGHTED-CLAUSE-REWRITING .
  pr EXTENDED-WEIGHTED-CLAUSE-TABLE-EXT .

  var Tr : ClauseInfo .
  vars CC CC1 CC2 : EWClauseTable .
  var M : Module .
  vars T1 T2 : Term .
  var GM : WAtomMagma .
  var DT : AtomMagma .
  var NeACS : NeAWClauseSet .
  vars ACS ACS1 ACS2 : AWClauseSet .
  vars C C1 C2 : WClause .
  vars MaxElem I MaxElem1 MaxElem2 : Nat .
  var CTT : EWClauseTable .
  vars CTL CTL1 CTL2 : EWClauseItemList .
  var NS : NatSet .
  var CTS : EWClauseSet .
  var NeCTS : NeEWClauseSet .
  var CTI : EWClauseItem .
    
  --- rewritables - obtain rewritables
  op rewritables : Module EWClause EWClauseTable -> Tuple{NatSet,EWClauseSet} .
  eq rewritables(M, (GM => (T1 = T2)) from Tr, CC) 
   = rewritables(M, CC, p2(applySelection(M, (GM => (T1 = T2))))) .
  eq rewritables(M, (GM => DT) from Tr, CC) 
   = ((empty).NatSet ; (empty).EWClauseSet) [owise] .

  op rewritables : Module EWClauseTable AWClauseSet -> Tuple{NatSet,EWClauseSet} .
  eq rewritables(M, (MaxElem, ((I, C1 from Tr) CTL)), NeACS)
    = if C1 =/= rewrite(M, NeACS, C1) 
      then ((I , p1 rewritables(M, (MaxElem, CTL), NeACS)) ; ((rewrite(M, NeACS, C1) from rewritten(I)), p2 rewritables(M, (MaxElem, CTL), NeACS))) 
      else (p1 rewritables(M, (MaxElem, CTL), NeACS) ; p2 rewritables(M, (MaxElem, CTL), NeACS)) 
      fi .
  eq rewritables(M, CC, ACS) = ((empty).NatSet ; (empty).EWClauseSet) [owise] .

--- dcRewritables - update deprecated clauses that are rewritables
  op dcRewritables : EWClauseTable EWClauseTable NatSet -> EWClauseTable .
  eq dcRewritables(CC1, CC2, (empty).NatSet) = CC2 .
  eq dcRewritables(CC1, CC2, (I , NS)) 
   = if getElemByIndex(I, CC1) == noElemError
     then dcRewritables(CC1, CC2, NS)
     else dcRewritables(removeIndexes(I,CC1), addInPosition(getElemByIndex(I, CC1),CC2), NS) 
     fi .
    
  --- cheap-rewrite - cheap rewriting on generated clauses
  op cheap-rewrite : Module AWClauseSet EWClauseSet -> EWClauseSet .
  eq cheap-rewrite(M, ACS, (empty).EWClauseSet)
   = (empty).EWClauseSet .
  eq cheap-rewrite(M, ACS, (GM => DT) from Tr)
   = cheap-rewrite(M, ACS, (GM => DT)) from Tr .
  eq cheap-rewrite(M, ACS, (((GM => DT) from Tr) , NeCTS))
      = cheap-rewrite(M, ACS, ((GM => DT) from Tr)) , cheap-rewrite(M, ACS, NeCTS) .

endfm
